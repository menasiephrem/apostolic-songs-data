$(document).ready(function () {
  const searchParams = new URLSearchParams(window.location.search)
  const id = searchParams.get('id')
  $.get(`https://api-as.miya.pro/lyrics/old/${id}`, function(data, status){
     $("#title").val(data.title);
     $("#lyrics").val(data.lyrics);
     $('#counter').text(data.id);
  });

  $("#song-form").submit(function (e) {
    e.preventDefault();
    let error = false;
    if ($('#title').val()) {
      $('#title').removeClass('input-error');
    } else {
      $('#title').addClass('input-error');
      error = true
    }
    if ($('#lyrics').val()) {
      $('#lyrics').removeClass('input-error');
    } else {
      $('#lyrics').addClass('input-error');
      error = true
    }
    if (!error) {
      var el = parseInt($('#counter').text());
      const data = {
        title: $('#title').val(),
        lyrics: $('#lyrics').val()
      }
      $.ajax({
        type: "PUT",
        url: `https://api-as.miya.pro/lyrics/old/${id}`,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function (data, status, jqXHR) {
           $("#title").val(data.title);
           $("#lyrics").val(data.lyrics);
           $("#sucess").text("Lyrics Sucessfully Updated!");
           $('#counter').text(data.id);
        },
        error: function (jqXHR, status) {
          // error handler
          console.log(jqXHR);
          alert('fail ' + status.message);
        }
      });

    }
  });

});

