$(document).ready(function () {
    const counter = window.localStorage.getItem("newCounter")
    $('#counter').text(counter || 57);
    $("#song-form").submit(function (e) {
      e.preventDefault();
      let error = false;
      if ($('#title').val()) {
        $('#title').removeClass('input-error');
      } else {
        $('#title').addClass('input-error');
        error = true
      }
      if ($('#lyrics').val()) {
        $('#lyrics').removeClass('input-error');
      } else {
        $('#lyrics').addClass('input-error');
        error = true
      }
      if (!error) {
        var el = parseInt($('#counter').text());
        const data = {
          title: $('#title').val(),
          lyrics: $('#lyrics').val(),
          id: el
        }
        $.ajax({
          type: "POST",
          url: "https://api-as.miya.pro/lyrics/createOld",
          data: JSON.stringify(data),
          contentType: "application/json; charset=utf-8",
          crossDomain: true,
          dataType: "json",
          success: function (data, status, jqXHR) {
             $("#title").val("");
             $("#lyrics").val("");
             $("#sucess").text("Lyrics Sucessfully Added!");
             $('#counter').text(el + 1);
             localStorage.setItem("newCounter", el + 1)
          },
          error: function (jqXHR, status) {
            // error handler
            console.log(jqXHR);
            alert('fail ' + status.message);
          }
        });
  
      }
    });
  
  });
  
  