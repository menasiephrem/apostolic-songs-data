$(function() {

  $(".numbers-row").append('<div class="inc button-img">+</div><div class="dec button-img">-</div>');

  $(".button-img").on("click", function() {

    var $button = $(this);
    var oldValue = parseInt($('#counter').text());

    if ($button.text() == "+") {
  	  var newVal = parseFloat(oldValue) + 1;
  	} else {
	   // Don't allow decrementing below zero
      if (oldValue > 0) {
        var newVal = parseFloat(oldValue) - 1;
	    } else {
        newVal = 0;
      }
	  }
    $('#counter').text(newVal);
    localStorage.setItem("newCounter", newVal)
  });

});