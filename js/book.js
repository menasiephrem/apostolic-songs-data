$(document).ready(function () {
  $.get("https://api-as.miya.pro/lyrics/old", function(data, status){
      
      let content = ""
      for(i=0; i<data.length; i++){

          const da = data[i];
          const book = new Date(da.date_created) < new Date("2020-08-20T15:28:51.415Z")? "መፅሐፍ ሁለት" : "መፅሐፍ አንድ"
          content += `<tr data="${da._id}" class="row100 body">`
          content += `<td class="cell100 column1">${book === "Book 2"? i + 1 : da.id}</td>`;
          content += `<td class="cell100 column2">${da.title}</td>`;
          content += `<td class="cell100 column3">${book}</td>`;
          content += `<td class="cell100 column4">${formatDate(da.date_created)}</td>`;
          content += `<td class="cell100 column5">${formatDate(da.last_modified)}</td>`;
          content +=  "</tr>"

      }

      $('.tboday-lyrics').append(content)
  });
 
  $(".lyrics-table").on("click", "tr", function() { 
      window.location.href = `lyrics.html?id=${$(this).attr('data')}`
  });

  $("#newsong").click(function(){
    window.location.href = "newLyrics.html"
  })

  function formatDate(da){
    const date = new Date(da)
    const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'short', day: '2-digit' }) 
    const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat .formatToParts(date ) 

    return `${day} ${month} ${year }`
  }

});